import logging
import unittest
import hashlib
import requests
from pyDownloader import pyDownloader
from pyDownloader.protocols.http import HTTPDownloader
from pyDownloader.protocols.exceptions import ProtocolNotConfiguredException
logging.basicConfig(level=logging.DEBUG)

# ======================================================================================================================
# Steps to get this tests running (Docker installation required and current user can create/remove images)
# ======================================================================================================================
# 1) Open a terminal in the tests/dockerfiles folder
# 2) Build the docker container for apache `docker build -f http_https/dockerfile -t pydownloader/http .`
# 3) Run the docker container for apache (Works for HTTP and HTTPS Protocol) `docker run -p 80:80 -p 443:443
#    pydownloader/http`
# 4) In Pycharm, run the unit tests for HTTP and HTTPS
# 5) ???
# 6) Profit
# 7) Once you are done tinkering with the HTTP and HTTPS protocol, press ctrl + c to close the apache docker instance
# 8 [ optional ]) If you want to remove the built image from docker: `docker -f rmi pydownloader/http`
# ======================================================================================================================
#TODO: Implement docker build + run in python

class TestHTTPDownloader(unittest.TestCase):
    # Authentication
    valid_auth_downloader_simple = HTTPDownloader('http://testuser:testpassword@example.com/simplepath',
                                                  '/tmp/unittest')

    valid_auth_downloader_complex = HTTPDownloader('http://testuser:testpassword@example.com/complex/path',
                                                   '/tmp/unittest')

    # No Authentication
    valid_no_auth_downloader_simple = HTTPDownloader('http://example.com/simplepath', '/tmp/unittest')
    valid_no_auth_downloader_complex = HTTPDownloader('http://example.com/complex/path', '/tmp/unittest')

    def test_initialization(self):
        self.assertEqual('testuser', self.valid_auth_downloader_simple.username)
        self.assertEqual('testpassword', self.valid_auth_downloader_simple.password)
        self.assertEqual('example.com', self.valid_auth_downloader_simple.host)
        self.assertEqual('simplepath', self.valid_auth_downloader_simple.path)

        self.assertEqual('testuser', self.valid_auth_downloader_complex.username)
        self.assertEqual('testpassword', self.valid_auth_downloader_complex.password)
        self.assertEqual('example.com', self.valid_auth_downloader_complex.host)
        self.assertEqual('complex/path', self.valid_auth_downloader_complex.path)

        self.assertEqual(None, self.valid_no_auth_downloader_simple.username)
        self.assertEqual(None, self.valid_no_auth_downloader_simple.password)
        self.assertEqual('example.com', self.valid_no_auth_downloader_simple.host)
        self.assertEqual('simplepath', self.valid_no_auth_downloader_simple.path)

        self.assertEqual(None, self.valid_no_auth_downloader_complex.username)
        self.assertEqual(None, self.valid_no_auth_downloader_complex.password)
        self.assertEqual('example.com', self.valid_no_auth_downloader_complex.host)
        self.assertEqual('complex/path', self.valid_no_auth_downloader_complex.path)

        with self.assertRaises(ProtocolNotConfiguredException):
            HTTPDownloader('http://testuser@example.com/simplepath', '/tmp/unittest')

        with self.assertRaises(ProtocolNotConfiguredException):
            HTTPDownloader('http://testuser:example.com/complex/path', '/tmp/unittest')

    def test_download_unauthenticated(self):
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'http://test.local/testfile'

        downloader = HTTPDownloader(remote_file, local_file)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_authenticated(self):
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'http://user:password@secure.test.local/testfile'

        downloader = HTTPDownloader(remote_file, local_file)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())


    def test_download_authenticated_main_script(self):
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'http://user:password@secure.test.local/testfile'

        downloader = pyDownloader.get_downloader(remote_file, local_file)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())


    def test_download_unauthenticated_read_timeout(self):
        local_file = '/tmp/file1'
        remote_file = 'http://test.local/bigtestfile'

        downloader = HTTPDownloader(remote_file, local_file)

        with self.assertRaises(requests.exceptions.ReadTimeout):
            downloader.download()

            print("Stop docker before end of download")

            while not downloader.completed():
                pass