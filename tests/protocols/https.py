import logging
import unittest
import hashlib
from pyDownloader import pyDownloader
from pyDownloader.protocols.https import HTTPSDownloader
from pyDownloader.protocols.exceptions import ProtocolNotConfiguredException
logging.basicConfig(level=logging.DEBUG)

# ======================================================================================================================
# Steps to get this tests running (Docker installation required and current user can create/remove images)
# ======================================================================================================================
# 1) Open a terminal in the tests/dockerfiles folder
# 2) Build the docker container for apache `docker build -f http_https/dockerfile -t pydownloader/http .`
# 3) Run the docker container for apache (Works for HTTP and HTTPS Protocol) `docker run -p 80:80 -p 443:443
#    pydownloader/http`
# 4) In Pycharm, run the unit tests for HTTP and HTTPS
# 5) ???
# 6) Profit
# 7) Once you are done tinkering with the HTTP and HTTPS protocol, press ctrl + c to close the apache docker instance
# 8 [ optional ]) If you want to remove the built image from docker: `docker -f rmi pydownloader/http`
# ======================================================================================================================
#TODO: Implement docker build + run in python


class TestHTTPSDownloader(unittest.TestCase):
    # Authentication
    valid_auth_downloader_simple = HTTPSDownloader('https://testuser:testpassword@example.com/simplepath',
                                                   '/tmp/unittest')
    valid_auth_downloader_complex = HTTPSDownloader('https://testuser:testpassword@example.com/complex/path',
                                                    '/tmp/unittest')

    # No Authentication
    valid_no_auth_downloader_simple = HTTPSDownloader('https://example.com/simplepath', '/tmp/unittest')
    valid_no_auth_downloader_complex = HTTPSDownloader('https://example.com/complex/path', '/tmp/unittest')

    def test_initialization(self):
        self.assertEqual('testuser', self.valid_auth_downloader_simple.username)
        self.assertEqual('testpassword', self.valid_auth_downloader_simple.password)
        self.assertEqual('example.com', self.valid_auth_downloader_simple.host)
        self.assertEqual('simplepath', self.valid_auth_downloader_simple.path)

        self.assertEqual('testuser', self.valid_auth_downloader_complex.username)
        self.assertEqual('testpassword', self.valid_auth_downloader_complex.password)
        self.assertEqual('example.com', self.valid_auth_downloader_complex.host)
        self.assertEqual('complex/path', self.valid_auth_downloader_complex.path)

        self.assertEqual(None, self.valid_no_auth_downloader_simple.username)
        self.assertEqual(None, self.valid_no_auth_downloader_simple.password)
        self.assertEqual('example.com', self.valid_no_auth_downloader_simple.host)
        self.assertEqual('simplepath', self.valid_no_auth_downloader_simple.path)

        self.assertEqual(None, self.valid_no_auth_downloader_complex.username)
        self.assertEqual(None, self.valid_no_auth_downloader_complex.password)
        self.assertEqual('example.com', self.valid_no_auth_downloader_complex.host)
        self.assertEqual('complex/path', self.valid_no_auth_downloader_complex.path)

        with self.assertRaises(ProtocolNotConfiguredException):
            HTTPSDownloader('https://testuser@example.com/simplepath', '/tmp/unittest')

        with self.assertRaises(ProtocolNotConfiguredException):
            HTTPSDownloader('https://testuser:example.com/complex/path', '/tmp/unittest')

    def test_download_unauthenticated(self):
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'https://test.local/testfile'

        downloader = HTTPSDownloader(remote_file, local_file, ssl_verify=False)

        downloader.download()
        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_authenticated(self):
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'https://user:password@secure.test.local/testfile'

        downloader = HTTPSDownloader(remote_file, local_file, ssl_verify=False)

        downloader.download()
        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_fail_bad_ssl_with_verify_true(self):
        local_file = '/tmp/file2'
        remote_file = 'https://test.local/testfile'

        downloader = HTTPSDownloader(remote_file, local_file, ssl_verify=True)

        downloader.download()
        while not downloader.downloading and not downloader.error:
            pass

        self.assertEqual(True, downloader.error)

    def test_download_authenticated_main_script(self):
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'https://user:password@secure.test.local/testfile'

        downloader = pyDownloader.get_downloader(remote_file, local_file, ssl_verify=False)

        downloader.download()
        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())